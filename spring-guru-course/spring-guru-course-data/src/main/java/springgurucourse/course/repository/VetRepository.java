package springgurucourse.course.repository;

import org.springframework.data.repository.CrudRepository;
import springgurucourse.course.model.Pet;
import springgurucourse.course.model.Vet;

public interface VetRepository extends CrudRepository<Vet, Long> {
}
