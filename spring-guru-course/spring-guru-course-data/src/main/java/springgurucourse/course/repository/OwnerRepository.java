package springgurucourse.course.repository;

import org.springframework.data.repository.CrudRepository;
import springgurucourse.course.model.Owner;

public interface OwnerRepository extends CrudRepository<Owner, Long> {
    
    Owner findByLastName(String lastName);
}
