package springgurucourse.course.repository;

import org.springframework.data.repository.CrudRepository;
import springgurucourse.course.model.Visit;

public interface VisitRepository extends CrudRepository<Visit, Long> {
}
