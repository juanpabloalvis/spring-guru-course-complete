package springgurucourse.course.repository;

import org.springframework.data.repository.CrudRepository;
import springgurucourse.course.model.PetType;

public interface PetTypeRepository extends CrudRepository<PetType, Long> {
}
