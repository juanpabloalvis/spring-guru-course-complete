package springgurucourse.course.repository;

import org.springframework.data.repository.CrudRepository;
import springgurucourse.course.model.Speciality;

public interface SpecialityRepository extends CrudRepository<Speciality, Long> {
}
