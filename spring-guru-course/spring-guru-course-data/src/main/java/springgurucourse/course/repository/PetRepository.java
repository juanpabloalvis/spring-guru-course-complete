package springgurucourse.course.repository;

import org.springframework.data.repository.CrudRepository;
import springgurucourse.course.model.Pet;

public interface PetRepository extends CrudRepository<Pet, Long> {
}
