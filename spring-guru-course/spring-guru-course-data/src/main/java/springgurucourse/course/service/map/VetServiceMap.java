package springgurucourse.course.service.map;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import springgurucourse.course.model.Speciality;
import springgurucourse.course.model.Vet;
import springgurucourse.course.service.SpecialityService;
import springgurucourse.course.service.VetService;

import java.util.Set;

@Service
@Profile({"default", "map"})
public class VetServiceMap extends AbstractMapService<Vet, Long> implements VetService {
    
    private final SpecialityService specialityService;

    public VetServiceMap(SpecialityService specialityService) {
        this.specialityService = specialityService;
    }

    @Override
    public Set<Vet> findAll() {
        return super.findAll();
    }

    @Override
    public void deleteById(Long id) {
        super.deleteById(id);
    }

    @Override
    public void delete(Vet object) {
        super.delete(object);

    }
    public Vet save(Vet object) {
        if (object != null) {
            if (object.getSpecialities().size() > 0) {
                object.getSpecialities().forEach(
                        speciality -> {
                            if (speciality.getId() != null) {
                                Speciality savedSpeciality = specialityService.save(speciality);
                                speciality.setId(savedSpeciality.getId());
                            } 

                        });
            }
            return super.save(object);
        }
        return null;
    }

    @Override
    public Vet findById(Long id) {
        return super.findById(id);
    }
}
