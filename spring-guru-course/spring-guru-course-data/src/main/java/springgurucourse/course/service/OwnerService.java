package springgurucourse.course.service;

import springgurucourse.course.model.Owner;

public interface OwnerService extends CrudService<Owner, Long> {
    
    Owner findByLastName(String lastName);
}
