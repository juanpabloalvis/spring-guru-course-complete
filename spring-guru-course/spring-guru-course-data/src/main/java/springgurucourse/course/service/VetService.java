package springgurucourse.course.service;

import springgurucourse.course.model.Vet;

/**
 * Created by jp on 12/28/19.
 */
public interface VetService extends CrudService<Vet, Long> {

}
