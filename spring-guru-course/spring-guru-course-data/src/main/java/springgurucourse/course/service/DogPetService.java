package springgurucourse.course.service;

import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import springgurucourse.course.model.Pet;

import java.util.Set;

/**
 * Created by jt on 12/28/19.
 */
@Primary
@Service
@Profile({"dog", "default"})
public class DogPetService implements PetService {
    public String getPetType() {
        return "Dogs are the best!";
    }

    @Override
    public Set<Pet> findAll() {
        return null;
    }

    @Override
    public Pet findById(Long aLong) {
        return null;
    }

    @Override
    public Pet save(Pet object) {
        return null;
    }

    @Override
    public void delete(Pet object) {

    }

    @Override
    public void deleteById(Long aLong) {

    }
}

