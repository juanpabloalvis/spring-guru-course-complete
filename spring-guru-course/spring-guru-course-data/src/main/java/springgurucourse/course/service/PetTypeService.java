package springgurucourse.course.service;

import springgurucourse.course.model.PetType;

/**
 * Created by jp on 12/28/19.
 */
public interface PetTypeService extends CrudService<PetType, Long> {


}
