package springgurucourse.course.service.springdata;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import springgurucourse.course.model.Speciality;
import springgurucourse.course.repository.SpecialityRepository;
import springgurucourse.course.service.SpecialityService;

import java.util.HashSet;
import java.util.Set;

@Service
@Profile("springdatajpa")
public class SpecialityServiceSD implements SpecialityService {

    private final SpecialityRepository vetRepository;

    public SpecialityServiceSD(SpecialityRepository vetRepository) {
        this.vetRepository = vetRepository;
    }

    @Override
    public Set<Speciality> findAll() {
        Set<Speciality> vets = new HashSet<>();
        vetRepository.findAll().forEach(vets::add);
        return vets;
    }

    @Override
    public Speciality findById(Long aLong) {
        // retorna un opcional, en tonces la abreviación, para hacer 
        // el get, es retorne el objeto o de lo contrario null
        return vetRepository.findById(aLong).orElse(null);
    }

    @Override
    public Speciality save(Speciality object) {
        return vetRepository.save(object);
    }

    @Override
    public void delete(Speciality object) {
        vetRepository.delete(object);
    }

    @Override
    public void deleteById(Long aLong) {
        vetRepository.deleteById(aLong);
    }
}
