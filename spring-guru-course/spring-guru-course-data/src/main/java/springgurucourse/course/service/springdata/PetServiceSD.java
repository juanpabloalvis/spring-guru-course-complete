package springgurucourse.course.service.springdata;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import springgurucourse.course.model.Pet;
import springgurucourse.course.repository.PetRepository;
import springgurucourse.course.service.PetService;

import java.util.HashSet;
import java.util.Set;

@Service
@Profile("springdatajpa")
public class PetServiceSD implements PetService {

    private final PetRepository vetRepository;

    public PetServiceSD(PetRepository vetRepository) {
        this.vetRepository = vetRepository;
    }

    @Override
    public Set<Pet> findAll() {
        Set<Pet> vets = new HashSet<>();
        vetRepository.findAll().forEach(vets::add);
        return vets;
    }

    @Override
    public Pet findById(Long aLong) {
        // retorna un opcional, en tonces la abreviación, para hacer 
        // el get, es retorne el objeto o de lo contrario null
        return vetRepository.findById(aLong).orElse(null);
    }

    @Override
    public Pet save(Pet object) {
        return vetRepository.save(object);
    }

    @Override
    public void delete(Pet object) {
        vetRepository.delete(object);
    }

    @Override
    public void deleteById(Long aLong) {
        vetRepository.deleteById(aLong);
    }

    @Override
    public String getPetType() {
        return null;
    }
}
