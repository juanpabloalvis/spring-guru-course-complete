package springgurucourse.course.service;

import org.springframework.stereotype.Service;

@Service
public class PropertyInjectedGreetingService implements GreetingService {
    public String sayGreeting() {
        return "Hola que tal! - property";
    }
}
