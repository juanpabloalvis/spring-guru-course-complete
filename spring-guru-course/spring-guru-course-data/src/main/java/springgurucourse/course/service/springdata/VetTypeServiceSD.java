package springgurucourse.course.service.springdata;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import springgurucourse.course.model.PetType;
import springgurucourse.course.repository.PetTypeRepository;
import springgurucourse.course.service.PetTypeService;

import java.util.HashSet;
import java.util.Set;

@Service
@Profile("springdatajpa")
public class VetTypeServiceSD implements PetTypeService {

    private final PetTypeRepository petTypeRepository;

    public VetTypeServiceSD(springgurucourse.course.repository.PetTypeRepository petTypeRepository) {
        this.petTypeRepository = petTypeRepository;
    }

    @Override
    public Set<PetType> findAll() {
        Set<PetType> petTypes = new HashSet<>();
        petTypeRepository.findAll().forEach(petTypes::add);
        return petTypes;
    }

    @Override
    public PetType findById(Long aLong) {
        return petTypeRepository.findById(aLong).orElseGet(null);
    }

    @Override
    public PetType save(PetType object) {
       return petTypeRepository.save(object);
    }

    @Override
    public void delete(PetType object) {
        petTypeRepository.delete(object);
    }

    @Override
    public void deleteById(Long aLong) {
        petTypeRepository.deleteById(aLong);
    }
}
