package springgurucourse.course.service.springdata;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import springgurucourse.course.model.Visit;
import springgurucourse.course.repository.VisitRepository;
import springgurucourse.course.service.VisitService;

import java.util.HashSet;
import java.util.Set;

@Service
@Profile("springdatajpa")
public class VisitServiceSD implements VisitService {

    private final VisitRepository vetRepository;

    public VisitServiceSD(VisitRepository vetRepository) {
        this.vetRepository = vetRepository;
    }

    @Override
    public Set<Visit> findAll() {
        Set<Visit> vets = new HashSet<>();
        vetRepository.findAll().forEach(vets::add);
        return vets;
    }

    @Override
    public Visit findById(Long aLong) {
        // retorna un opcional, en tonces la abreviación, para hacer 
        // el get, es retorne el objeto o de lo contrario null
        return vetRepository.findById(aLong).orElse(null);
    }

    @Override
    public Visit save(Visit object) {
        return vetRepository.save(object);
    }

    @Override
    public void delete(Visit object) {
        vetRepository.delete(object);
    }

    @Override
    public void deleteById(Long aLong) {
        vetRepository.deleteById(aLong);
    }

    
}
