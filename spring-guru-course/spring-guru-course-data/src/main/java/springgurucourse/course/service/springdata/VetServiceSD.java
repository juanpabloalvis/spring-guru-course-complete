package springgurucourse.course.service.springdata;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import springgurucourse.course.model.Vet;
import springgurucourse.course.repository.VetRepository;
import springgurucourse.course.service.VetService;

import java.util.HashSet;
import java.util.Set;

@Service
@Profile("springdatajpa")
public class VetServiceSD implements VetService {

    private final VetRepository vetRepository;

    public VetServiceSD(VetRepository vetRepository) {
        this.vetRepository = vetRepository;
    }

    @Override
    public Set<Vet> findAll() {
        Set<Vet> vets = new HashSet<>();
        vetRepository.findAll().forEach(vets::add);
        return vets;
    }

    @Override
    public Vet findById(Long aLong) {
        // retorna un opcional, en tonces la abreviación, para hacer 
        // el get, es retorne el objeto o de lo contrario null
        return vetRepository.findById(aLong).orElse(null);
    }

    @Override
    public Vet save(Vet object) {
        return vetRepository.save(object);
    }

    @Override
    public void delete(Vet object) {
        vetRepository.delete(object);
    }

    @Override
    public void deleteById(Long aLong) {
        vetRepository.deleteById(aLong);
    }
}
