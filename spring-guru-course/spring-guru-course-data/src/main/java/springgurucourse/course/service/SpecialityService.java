package springgurucourse.course.service;

import springgurucourse.course.model.Speciality;

/**
 * Created by jp on 12/28/19.
 */
public interface SpecialityService extends CrudService<Speciality, Long> {

    
}
