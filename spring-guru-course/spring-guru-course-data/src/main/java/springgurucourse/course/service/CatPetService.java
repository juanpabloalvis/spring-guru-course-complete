package springgurucourse.course.service;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import springgurucourse.course.model.Pet;

import java.util.Set;

/**
 * Created by jt on 12/28/19.
 */
@Service
@Profile({"default","cat"})
public class CatPetService implements PetService {
    @Override
    public String getPetType() {
        return "Cats Are the Best!";
    }

    @Override
    public Set<Pet> findAll() {
        return null;
    }

    @Override
    public Pet findById(Long aLong) {
        return null;
    }

    @Override
    public Pet save(Pet object) {
        return null;
    }

    @Override
    public void delete(Pet object) {

    }

    @Override
    public void deleteById(Long aLong) {

    }
}
