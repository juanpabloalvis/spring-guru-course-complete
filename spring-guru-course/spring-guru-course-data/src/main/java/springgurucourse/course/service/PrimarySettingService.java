package springgurucourse.course.service;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Primary // Tendra precedencia cuando no hay un qualifier definido 
@Service
public class PrimarySettingService implements GreetingService {
    @Override
    public String sayGreeting() {
        return "Hola desde un Bean PRIMARIO";
    }
}
