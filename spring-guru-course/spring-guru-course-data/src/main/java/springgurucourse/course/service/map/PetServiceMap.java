package springgurucourse.course.service.map;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import springgurucourse.course.model.Pet;
import springgurucourse.course.service.CrudService;
import springgurucourse.course.service.PetService;

import java.util.Set;

@Service
@Profile({"default", "map"})
public class PetServiceMap extends AbstractMapService<Pet, Long> implements PetService {
    @Override
    public Set<Pet> findAll() {
        return super.findAll();
    }

    @Override
    public void deleteById(Long id) {
        super.deleteById(id);
    }

    @Override
    public void delete(Pet object) {
        super.delete(object);

    }

    @Override
    public Pet save(Pet object) {
        return super.save(object);
    }

    @Override
    public Pet findById(Long id) {
        return super.findById(id);
    }

    @Override
    public String getPetType() {
        return null;
    }
}
