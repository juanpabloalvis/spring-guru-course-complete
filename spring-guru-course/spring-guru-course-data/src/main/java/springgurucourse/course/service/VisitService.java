package springgurucourse.course.service;

import springgurucourse.course.model.Visit;

public interface VisitService extends CrudService<Visit, Long> {
}
