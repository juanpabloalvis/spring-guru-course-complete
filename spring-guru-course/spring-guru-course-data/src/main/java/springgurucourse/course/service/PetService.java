package springgurucourse.course.service;

import springgurucourse.course.model.Pet;

/**
 * Created by jp on 12/28/19.
 */
public interface PetService extends CrudService<Pet, Long> {

    String getPetType();
}
