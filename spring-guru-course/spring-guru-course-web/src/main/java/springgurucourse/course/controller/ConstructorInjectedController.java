package springgurucourse.course.controller;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import springgurucourse.course.service.GreetingService;

@Controller
public class ConstructorInjectedController {
    private GreetingService greetingService;

    // Spring does not know what Implementation of GreetingService use, so we use qualifier
    public ConstructorInjectedController(@Qualifier("constructorGreetingService") GreetingService greetingService) {
        this.greetingService = greetingService;
    }

    public String getGreeting() {
        return greetingService.sayGreeting();
    }

}
