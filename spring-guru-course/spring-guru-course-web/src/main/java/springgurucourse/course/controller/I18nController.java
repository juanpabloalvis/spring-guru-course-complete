package springgurucourse.course.controller;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import springgurucourse.course.service.GreetingService;

@Controller
public class I18nController {
    
    private final GreetingService greetingService;

    public I18nController(@Qualifier("constructorGreetingService") GreetingService greetingService) {
        //public I18nController(GreetingService greetingService) {
        this.greetingService = greetingService;
    }

    public String sayGreeting() {
        return greetingService.sayGreeting();
    }
}
