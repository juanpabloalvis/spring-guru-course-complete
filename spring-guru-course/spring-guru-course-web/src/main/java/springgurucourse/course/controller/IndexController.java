package springgurucourse.course.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {

    @RequestMapping({"", "/", "index", "index.html"})
    public String index(Model model) {
        //System.out.println("fyokls");
        model.addAttribute("wellcome", "Hola que tal, bienvenido");

        return "index";
    }


    @RequestMapping("/oops")
    public String oops(Model model) {
        return "notimplemented";
    }
}
