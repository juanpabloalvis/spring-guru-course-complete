package springgurucourse.course.controller;

import org.springframework.stereotype.Controller;
import springgurucourse.course.service.GreetingService;


@Controller
public class MyController {

    // it does not use qualifier because the bean implementation has PrimaryBean Annotation
    private final GreetingService greetingService;

    public MyController(GreetingService greetingService) {
        this.greetingService = greetingService;
    }

    public String sayHola() {
        return greetingService.sayGreeting();
    }
}
