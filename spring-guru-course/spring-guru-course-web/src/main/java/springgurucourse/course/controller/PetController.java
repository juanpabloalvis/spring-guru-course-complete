package springgurucourse.course.controller;

import org.springframework.stereotype.Controller;
import springgurucourse.course.service.PetService;


/**
 * Created by jp on 12/28/19.
 */
@Controller
public class PetController {

    private final PetService petService;

    public PetController(PetService petService) {
        this.petService = petService;
    }

    public String whichPetIsTheBest() {
        return petService.getPetType();
    }
}
