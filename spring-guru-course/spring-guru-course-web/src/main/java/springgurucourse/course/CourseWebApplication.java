package springgurucourse.course;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import springgurucourse.course.controller.ConstructorInjectedController;
import springgurucourse.course.controller.I18nController;
import springgurucourse.course.controller.MyController;
import springgurucourse.course.controller.PropertyInjectedController;
import springgurucourse.course.controller.SetterInjectedController;


@SpringBootApplication
public class CourseWebApplication {


    public static void main(String[] args) {
        //SpringApplication.run(CourseApplication.class, args);

        ApplicationContext applicationContext = SpringApplication.run(CourseWebApplication.class, args);

//        I18nController i18nController = (I18nController) applicationContext.getBean("i18nController");
//        System.out.println("------- Profile");
//        System.out.println(i18nController.sayGreeting());

        MyController myController = (MyController) applicationContext.getBean("myController");

        System.out.println("------- Primary Bean");
        System.out.println(myController.sayHola());

        System.out.println("------ Property");
        PropertyInjectedController propertyInjectedController = (PropertyInjectedController) applicationContext.getBean("propertyInjectedController");
        System.out.println(propertyInjectedController.getGreeting());

        System.out.println("--------- Setter");
        SetterInjectedController setterInjectedController = (SetterInjectedController) applicationContext.getBean("setterInjectedController");
        System.out.println(setterInjectedController.getGretting());

        System.out.println("-------- Constructor");
        ConstructorInjectedController constructorInjectedController = (ConstructorInjectedController) applicationContext.getBean("constructorInjectedController");
        System.out.println(constructorInjectedController.getGreeting());
    }

}
