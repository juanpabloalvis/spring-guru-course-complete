package springgurucourse.course.bootstrap;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import springgurucourse.course.model.Owner;
import springgurucourse.course.model.Pet;
import springgurucourse.course.model.PetType;
import springgurucourse.course.model.Speciality;
import springgurucourse.course.model.Vet;
import springgurucourse.course.model.Visit;
import springgurucourse.course.service.OwnerService;
import springgurucourse.course.service.PetTypeService;
import springgurucourse.course.service.SpecialityService;
import springgurucourse.course.service.VetService;
import springgurucourse.course.service.VisitService;

import java.time.LocalDate;

/**
 * Data initializer (implements CommandLineRunner)
 * Haciendolo componente, lo que ocurre es que cuando esté arriba totalmente el contexto de spring, se ejecutará este runner.
 */
@Component
public class DataLoad implements CommandLineRunner {

    private final OwnerService ownerService;
    private final VetService vetService;
    private final PetTypeService petTypeService;
    private final SpecialityService specialityService;
    private final VisitService visitService;

    //@Autowired no es requerido cuando uso constructor
    public DataLoad(OwnerService ownerService, VetService vetService, PetTypeService petTypeService, SpecialityService specialityService, VisitService visitService) {
        this.ownerService = ownerService;
        this.vetService = vetService;
        this.petTypeService = petTypeService;
        this.specialityService = specialityService;
        this.visitService = visitService;
    }

    @Override
    public void run(String... args) throws Exception {

        int count = petTypeService.findAll().size();

        if (count == 0) {
            loadData();
            System.out.println("Loaded owners and vets");
        }

    }

    private void loadData() {
        PetType dog = new PetType();
        dog.setName("Dog");
        PetType savedDogPetType = petTypeService.save(dog);

        PetType cat = new PetType();
        cat.setName("Cat");
        PetType savedCatPetType = petTypeService.save(cat);

        Speciality radiology = new Speciality();
        radiology.setDescription("Radiology");
        specialityService.save(radiology);

        Speciality surgery = new Speciality();
        radiology.setDescription("Surgery");
        specialityService.save(surgery);

        Speciality dentist = new Speciality();
        radiology.setDescription("dentist");
        specialityService.save(dentist);

        Owner owner1 = Owner.builder().firstName("Cleotilde").lastName("Fernandez").address("123 Brunwich St").telephone("123456789").city("Brisbane").build();
        Pet cleotildesPet = new Pet();
        cleotildesPet.setPetType(savedDogPetType);
        cleotildesPet.setBirdhDate(LocalDate.now());
        cleotildesPet.setOwner(owner1);
        cleotildesPet.setName("Rosco");
        owner1.getPets().add(cleotildesPet);

        ownerService.save(owner1);

        Owner owner2 = Owner.builder().firstName("Roberto").lastName("Gomez").address("456 Brunwich St").telephone("9876541").city("Brisbane").build();

        Pet robertoCat = new Pet();
        robertoCat.setPetType(savedCatPetType);
        robertoCat.setOwner(owner2);
        robertoCat.setBirdhDate(LocalDate.now());
        robertoCat.setName("Michifu");
        owner2.getPets().add(robertoCat);

        ownerService.save(owner2);

        Visit catVisit = new Visit();
        catVisit.setPet(robertoCat);
        catVisit.setDate(LocalDate.now());

        Owner owner3 = Owner.builder().firstName("Godinez").lastName("Martinez").address("123 Brunwich St").telephone("123456789").city("Brisbane").build();
        ownerService.save(owner3);
        
        Vet vet1 = new Vet();
        vet1.setFirstName("Sam");
        vet1.setLastName("Axe");
        vet1.getSpecialities().add(radiology);
        vet1.getSpecialities().add(surgery);
        vetService.save(vet1);

        Vet vet2 = new Vet();
        vet2.setFirstName("Yessie");
        vet2.setLastName("Porter");
        vet2.getSpecialities().add(surgery);
        vet2.getSpecialities().add(dentist);
        vetService.save(vet2);
    }
}
