package springgurucourse.course.service.springdata;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import springgurucourse.course.model.Owner;
import springgurucourse.course.repository.OwnerRepository;
import springgurucourse.course.repository.PetRepository;
import springgurucourse.course.repository.PetTypeRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

// in Junit 4 there are runners en Junit 5 there are extension
@ExtendWith(SpringExtension.class)
@SpringBootTest
class OwnerServiceSDTest {

    public static final long OWNER_ID = 1L;
    public static final String LAST_NAME = "Smith";

    @Mock
    private OwnerRepository ownerRepository;

    @Mock
    private PetRepository petRepository;

    @Mock
    private PetTypeRepository petTypeRepository;

    @InjectMocks
    private OwnerServiceSD serviceSD;

    private Owner returnOwner;

    @BeforeEach
    void setUp() {
        returnOwner = Owner.builder().id(OWNER_ID).lastName(LAST_NAME).build();
    }

    @Test
    void findByLastName() {

        when(ownerRepository.findByLastName(any())).thenReturn(returnOwner);

        Owner smith = serviceSD.findByLastName("Smith");

        assertNotNull(smith);
        assertEquals(OWNER_ID, smith.getId());
        assertEquals(LAST_NAME, smith.getLastName());

        verify(ownerRepository).findByLastName(any());

    }

    @Test
    void findAll() {
        Set<Owner> returnOwnersSet = new HashSet<>();
        returnOwnersSet.add(returnOwner);
        returnOwnersSet.add(Owner.builder().id(2L).build());
        when(ownerRepository.findAll()).thenReturn(returnOwnersSet);

        Set<Owner> owners = serviceSD.findAll();
        assertNotNull(owners);
        assertEquals(returnOwnersSet, owners);
        assertEquals(2, owners.size());
        verify(ownerRepository).findAll();

    }

    @Test
    void findById() {

        when(ownerRepository.findById(anyLong())).thenReturn(Optional.of(returnOwner));

        Owner smith = serviceSD.findById(OWNER_ID);

        assertNotNull(smith);
        assertEquals(OWNER_ID, smith.getId());

        verify(ownerRepository).findById(any());

    }


    @Test
    void findByIdNotFound() {

        when(ownerRepository.findById(anyLong())).thenReturn(Optional.empty());

        Owner empty = serviceSD.findById(OWNER_ID);

        assertNull(empty);
        verify(ownerRepository).findById(any());

    }

    @Test
    void save() {

        Owner owner = Owner.builder().id(1L).build();
        when(ownerRepository.save(any())).thenReturn(returnOwner);
        Owner saved = serviceSD.save(owner);
        assertNotNull(saved);
        assertEquals(OWNER_ID, saved.getId());

        verify(ownerRepository).save(any());
        
    }

    @Test
    void delete() {

        serviceSD.delete(returnOwner);
        verify(ownerRepository).delete(any());

    }

    @Test
    void deleteById() {
        serviceSD.deleteById(OWNER_ID);

        verify(ownerRepository).deleteById(any());

    }
}