package springgurucourse.course.controller;

import org.junit.Before;
import org.junit.Test;
import springgurucourse.course.service.PropertyInjectedGreetingService;

public class PropertyInjectedControllerTest {
    PropertyInjectedController controller;

    @Before
    public void setUp() {
        controller = new PropertyInjectedController();
        controller.greetingService = new PropertyInjectedGreetingService();
    }

    @Test
    public void getGreeting() {
        System.out.println(controller.getGreeting());
    }
}