package springgurucourse.course.controller;

import org.junit.Before;
import org.junit.Test;
import springgurucourse.course.service.ConstructorGreetingService;

public class ConstructorInjectedControllerTest {
    ConstructorInjectedController controller;

    @Before
    public void setUp() {
        controller = new ConstructorInjectedController(new ConstructorGreetingService());
    }

    @Test
    public void getGreeting() {
        System.out.println(controller.getGreeting());
    }
}