package springgurucourse.course.controller;

import org.junit.Before;
import org.junit.Test;
import springgurucourse.course.service.SetterInjectedGreetingService;

public class SetterInjectedControllerTest {

    SetterInjectedController controller;

    @Before
    public void setUp() {
        controller = new SetterInjectedController();
        controller.setGreetingService(new SetterInjectedGreetingService());
    }

    @Test
    public void getGreeting() {
        System.out.println(controller.getGretting());

    }
}