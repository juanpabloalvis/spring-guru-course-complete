package springguruconfiguration.configapp.service;

public interface GreetingService {
    String sayGreeting();
}
