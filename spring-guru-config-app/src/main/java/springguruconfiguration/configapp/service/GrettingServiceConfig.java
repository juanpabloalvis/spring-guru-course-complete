package springguruconfiguration.configapp.service;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GrettingServiceConfig {
    
    // Al definirlo como bean de spring, el elejirá el repositorio en tiempo de ejecución,
    // e inyectará la configuración y retornará una nueva instancia
    @Bean
    GreetingServiceFactory greetingServiceFactory(GreetingRepository repository){
        return new GreetingServiceFactory(repository);
    }
    
}
