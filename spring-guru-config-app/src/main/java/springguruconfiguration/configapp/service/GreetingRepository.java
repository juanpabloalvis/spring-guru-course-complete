package springguruconfiguration.configapp.service;

public interface GreetingRepository {
    String getEnglishGreeting();

    String getSpanishGreeting();

    String getGermanGreeting();
}
