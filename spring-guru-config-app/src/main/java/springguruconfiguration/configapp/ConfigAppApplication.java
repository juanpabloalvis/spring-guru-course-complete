package springguruconfiguration.configapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import springguruconfiguration.configapp.controller.ConstructorInjectedController;
import springguruconfiguration.configapp.controller.GetterInjectedController;
import springguruconfiguration.configapp.controller.MyController;
import springguruconfiguration.configapp.controller.PropertyInjectedController;
import springguruconfiguration.configapp.examplebeans.FakeDataSource;
import springguruconfiguration.configapp.examplebeans.FakeJmsBroker;

@SpringBootApplication
public class ConfigAppApplication {

    public static void main(String[] args) {
//		SpringApplication.run(ConfigAppApplication.class, args);
        ApplicationContext ctx = SpringApplication.run(ConfigAppApplication.class, args);

        MyController controller = (MyController) ctx.getBean("myController");

        System.out.println(controller.hello());
        System.out.println(ctx.getBean(PropertyInjectedController.class).sayHello());
        System.out.println(ctx.getBean(GetterInjectedController.class).sayHello());
        System.out.println(ctx.getBean(ConstructorInjectedController.class).sayHello());


        //FakeDataSource fakeDataSource = (FakeDataSource) ctx.getBean("fakeDataSource");
        FakeDataSource fakeDataSource = ctx.getBean(FakeDataSource.class);
        System.out.println("\n******" + fakeDataSource.getUser());
        System.out.println("\n******" + fakeDataSource.getPassword());
        System.out.println("\n******" + fakeDataSource.getUrl());

        FakeJmsBroker fakeJmsBroker = ctx.getBean(FakeJmsBroker.class);
        System.out.println("\n**jms**" + fakeJmsBroker.getUser());
        System.out.println("\n**jms**" + fakeJmsBroker.getPassword());
        System.out.println("\n**jms**" + fakeJmsBroker.getUrl()); 

    }

}
