package com.prueba.parameta.prueba.service;

import com.prueba.parameta.prueba.model.Employee;

public interface EmployeeService {
    Employee addEmployee(Employee employee);
}
