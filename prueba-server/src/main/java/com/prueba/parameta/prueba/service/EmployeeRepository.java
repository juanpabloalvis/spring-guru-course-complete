package com.prueba.parameta.prueba.service;

import com.prueba.parameta.prueba.model.Employee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public interface EmployeeRepository extends CrudRepository<Employee, Long> {
    
}
