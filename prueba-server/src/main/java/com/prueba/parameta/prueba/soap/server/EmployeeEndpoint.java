package com.prueba.parameta.prueba.soap.server;

import com.prueba.parameta.prueba.service.EmployeeService;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import com.prueba.parameta.prueba.model.Employee;
import parameta.prueba.GetEmployeeRequest;
import parameta.prueba.GetEmployeeResponse;

import java.time.LocalDate;

@Endpoint
public class EmployeeEndpoint {
    private static final String NAMESPACE_URI = "http://parameta/prueba.com";

    private EmployeeService employeeService;

    public EmployeeEndpoint(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getEmployeeRequest")
    @ResponsePayload
    public GetEmployeeResponse getEmployee(@RequestPayload GetEmployeeRequest request) {
        GetEmployeeResponse response = new GetEmployeeResponse();

        parameta.prueba.Employee employeeWsdl = request.getEmployee();
        Employee employee = mapEmployeeWsdlToEmployee(employeeWsdl);

        Employee employee1 = employeeService.addEmployee(employee);

        response.setId(employee1.getId());

        // TODO mapear todo el objeto persistido
        response.setEmployee(employeeWsdl);

        return response;
    }


    private Employee mapEmployeeWsdlToEmployee(parameta.prueba.Employee employeeWsdl) {
        Employee employee = new Employee();
        // TODO esto se puede mejorar con un mapper 
        employee.setNames(employeeWsdl.getNames());
        employee.setLastnames(employeeWsdl.getLastnames());
        employee.setDocumentType(employeeWsdl.getDocumentType());
        employee.setDocumentNumber(new Integer(employeeWsdl.getDocumentNumber()));
        employee.setBirthDate(LocalDate.parse(employeeWsdl.getBirthDate()));
        employee.setStartDate(LocalDate.parse(employeeWsdl.getStartDate()));
        employee.setPosition(employeeWsdl.getPosition());
        employee.setSalary(new Double(employeeWsdl.getSalary()));
        return employee;
    }
}
