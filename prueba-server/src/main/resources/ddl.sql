
		
create table employee (
id BIGINT NOT NULL AUTO_INCREMENT, 
birth_date date, 
document_number integer, 
document_type varchar(255), 
last_names varchar(255), 
names varchar(255), 
position varchar(255), 
salary double, 
start_date date, 
primary key (id));