package com.prueba.parameta.prueba.controller;

import com.prueba.parameta.prueba.GetEmployeeResponse;
import com.prueba.parameta.prueba.model.Employee;
import com.prueba.parameta.prueba.soap.client.EmployeeClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.Period;
import java.util.HashMap;

@RestController
public class EmployeeController {

    @Autowired
    private EmployeeClient employeeClient;


    // Creo que debería ser un POST
    @GetMapping("/employees")
    public ResponseEntity<Object> response(@RequestParam(required = false) String names,
                                           @RequestParam(required = false) String lastNames,
                                           @RequestParam(required = false) String docuemntType,
                                           @RequestParam(required = false) Integer documentNumber,
                                           @RequestParam(required = false) String birthDate,
                                           @RequestParam(required = false) String startDate,
                                           @RequestParam(required = false) String position,
                                           @RequestParam(required = false) Double salary) {

        LocalDate ldate = getLocalDate(birthDate);
        LocalDate sDate = getLocalDate(startDate);

        if (ldate == null || ldate.isAfter(LocalDate.now().minusYears(18))) {
            return getCustomErrorMessage("Fecha de nacimiento inválida, debe ser mayor de edad");
        }
        if (sDate == null) {
            return getCustomErrorMessage("La fecha de inicio no es válida");
        }

        if (null == names || null == lastNames || null == docuemntType || null == documentNumber || null == birthDate || null == startDate || null == position || null == salary) {
            return getCustomErrorMessage("Almenos un parámetro está vacío o no no es válido");

        }


        Employee employee = new Employee();
        employee.setNames(names);
        employee.setLastnames(lastNames);
        employee.setDocumentType(docuemntType);
        employee.setDocumentNumber(documentNumber);
        employee.setBirthDate(ldate);
        employee.setStartDate(sDate);
        employee.setPosition(position);
        employee.setSalary(salary);

        GetEmployeeResponse getEmployeeResponse = employeeClient.addEmployeeResponse(employee);
        com.prueba.parameta.prueba.Employee employee1 = getEmployeeResponse.getEmployee();

        Employee employeeResponse = mapEmployeeWsdlToEmployee(employee1);
        employeeResponse.setId(getEmployeeResponse.getId());

        HashMap<Object, Object> respuesta = new HashMap<>();
        respuesta.put("empleado", employeeResponse);
        respuesta.put("tiempoVinculación", getTimeLinkingToCompany(employeeResponse));
        respuesta.put("edadEmpleado", getEmployeeAge(employeeResponse));
        return new ResponseEntity<>(respuesta, HttpStatus.OK);
    }

    private String getEmployeeAge(Employee employeeResponse) {

        LocalDate today = LocalDate.now();
        Period period = Period.between(employeeResponse.getBirthDate(), today);

        return String.format("Edad actual del empleado es (%s años, %s meses, %s dias)", period.getYears(), period.getMonths(), period.getDays());
    }

    private String getTimeLinkingToCompany(Employee employeeResponse) {
        LocalDate today = LocalDate.now();
        Period period = Period.between(employeeResponse.getStartDate(), today);
        
        return String.format("Tiempo de Vinculación a la compañía es (%s años, %s meses, %s dias)", period.getYears(), period.getMonths(), period.getDays());
    }

    private LocalDate getLocalDate(String date) {
        try {
            return LocalDate.parse(date);
        } catch (Exception e) {
            return null;
        }
    }

    private Employee mapEmployeeWsdlToEmployee(com.prueba.parameta.prueba.Employee employeeWsdl) {
        Employee employee = new Employee();
        // TODO esto se puede mejorar con un mapper y no duplicar código
        employee.setNames(employeeWsdl.getNames());
        employee.setLastnames(employeeWsdl.getLastnames());
        employee.setDocumentType(employeeWsdl.getDocumentType());
        employee.setBirthDate(LocalDate.parse(employeeWsdl.getBirthDate()));
        employee.setDocumentNumber(new Integer(employeeWsdl.getDocumentNumber()));
        employee.setStartDate(LocalDate.parse(employeeWsdl.getStartDate()));
        employee.setPosition(employeeWsdl.getPosition());
        employee.setSalary(new Double(employeeWsdl.getSalary()));
        return employee;
    }

    private ResponseEntity<Object> getCustomErrorMessage(String errorMessage) {

        HashMap<Object, Object> error = new HashMap<>();
        error.put("errorMessage", errorMessage);
        error.put("code", HttpStatus.BAD_REQUEST.value());

        return
                new ResponseEntity<>(error, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

}
