
package com.prueba.parameta.prueba.soap.client;

import com.prueba.parameta.prueba.GetEmployeeRequest;
import com.prueba.parameta.prueba.GetEmployeeResponse;
import com.prueba.parameta.prueba.model.Employee;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;


public class EmployeeClient extends WebServiceGatewaySupport {

    private static final Logger log = LoggerFactory.getLogger(EmployeeClient.class);

    public GetEmployeeResponse addEmployeeResponse(Employee employee) {

        GetEmployeeRequest request = new GetEmployeeRequest();

        com.prueba.parameta.prueba.Employee employeeService = mapEmployeeToEmployeeWsdl(employee);

        request.setEmployee(employeeService);

        log.info("Requesting location for " + employee.getNames());

        GetEmployeeResponse response = (GetEmployeeResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:9898/parameta/ws/employees", request,
                        new SoapActionCallback(
                                "http://parameta/prueba.com/GetEmployeeRequest"));

        return response;
    }

    private com.prueba.parameta.prueba.Employee mapEmployeeToEmployeeWsdl(Employee employee) {

        com.prueba.parameta.prueba.Employee employeeService = new com.prueba.parameta.prueba.Employee();

        // TODO esto se puede mejorar con un mapper
        employeeService.setNames(employee.getNames());
        employeeService.setLastnames(employee.getLastnames());
        employeeService.setDocumentType(employee.getDocumentType());
        employeeService.setBirthDate(employee.getBirthDate().toString());
        employeeService.setDocumentNumber(employee.getDocumentNumber().toString());
        employeeService.setStartDate(employee.getStartDate().toString());
        employeeService.setPosition(employee.getPosition());
        employeeService.setSalary(employee.getSalary().toString());
        return employeeService;

    }

}
