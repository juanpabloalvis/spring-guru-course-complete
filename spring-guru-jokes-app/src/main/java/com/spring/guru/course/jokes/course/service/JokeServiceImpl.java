package com.spring.guru.course.jokes.course.service;

import guru.springframework.norris.chuck.ChuckNorrisQuotes;
import org.springframework.stereotype.Service;

@Service
public class JokeServiceImpl implements JokeService {

    private final ChuckNorrisQuotes chuckNorrisQuotes;

    // No se puede hacer autowire únicamente, poruqe no conoce el bean porque no ha sido creado, entonces 
    // no sepuede inyectar, por tanto se debe declarar en
    // com/spring/guru/course/jokes/course/config/ChuckConfiguration.java:11
    public JokeServiceImpl(ChuckNorrisQuotes chuckNorrisQuotes) {
        this.chuckNorrisQuotes = chuckNorrisQuotes;
    }

    @Override
    public String getJoke() {
        return chuckNorrisQuotes.getRandomQuote();
    }
}
