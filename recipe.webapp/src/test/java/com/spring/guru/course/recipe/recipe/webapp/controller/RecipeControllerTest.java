package com.spring.guru.course.recipe.recipe.webapp.controller;

import com.spring.guru.course.recipe.recipe.webapp.domain.Recipe;
import com.spring.guru.course.recipe.recipe.webapp.repository.CategoryRepository;
import com.spring.guru.course.recipe.recipe.webapp.repository.UnitOfMeasureRepository;
import com.spring.guru.course.recipe.recipe.webapp.service.RecipeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.Model;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;


public class RecipeControllerTest {


    private ReceipeController recipeController;

    @Mock
    private RecipeService recipeService;
    @Mock
    private Model model;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        recipeController = new ReceipeController(recipeService);

    }


    @Test
    public void testMockMVC() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(recipeController).build();

        mockMvc.perform(get("/receipe/show/1"))
                .andExpect(status().isOk())
                .andExpect(view()
                        .name("recipe/show"));

    }


    @Test
    public void getReceipeById() throws Exception {

        Recipe recipe = new Recipe();
        recipe.setId(1L);
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(recipeController).build();

        when(recipeService.findById(anyLong())).thenReturn(recipe);

        mockMvc.perform(get("/receipe/show/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("recipe/show"));


    }
}