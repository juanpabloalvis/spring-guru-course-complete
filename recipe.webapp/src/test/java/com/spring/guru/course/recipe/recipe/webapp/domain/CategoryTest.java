package com.spring.guru.course.recipe.recipe.webapp.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CategoryTest {

    private Category category;

    @BeforeEach
    public void setUp() throws Exception {
        category = new Category();
    }


    @Test
    void getId() {
        Long expected = 4L;
        category.setId(expected);
        assertEquals(expected, category.getId());
    }

    @Test
    void getDescription() {
        String expected = "Category Test";
        category.setDescription(expected);
        assertEquals(expected, category.getDescription());
    }

    @Test
    void getRecipes() {

        Set<Recipe> expectedRecipes = new HashSet<>();
        Recipe recipe = new Recipe();

        expectedRecipes.add(recipe);

        category.setRecipes(expectedRecipes);
        assertEquals(expectedRecipes, category.getRecipes());
    }
}