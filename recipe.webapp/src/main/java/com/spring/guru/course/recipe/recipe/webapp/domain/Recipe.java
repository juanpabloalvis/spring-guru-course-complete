package com.spring.guru.course.recipe.recipe.webapp.domain;

import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
public class Recipe {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String description;
    private Integer prepTime;
    private Integer cookTime;
    private Integer servings;
    private String source;
    private String url;
    
    @Lob
    private String directions;

    // crea un binario "lage" en la bd
    @Lob
    private Byte[] image;

    // por defecto ordinal es númerico, y para este caso lo hacemos como String ya que podremos 
    // agregar propiedades ordenados alfabeticamente 
    @Enumerated(value = EnumType.STRING)
    private Difficulty difficulty;

    // si persistimos, el baja hasta las notas y las guarda en cascada,
    // al igual que si eliminamos
    @OneToOne(cascade = CascadeType.ALL)
    private Notes notes;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "recipe")
    private Set<Ingredient> ingredients = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "recipe_category", // tabla intermedia
            joinColumns = @JoinColumn(name = "recipe_id"), //campo id entidad actual en tabla intermedia
            inverseJoinColumns = @JoinColumn(name = "category_id")) //campo id entidad remota en tabla intermedia además, la entidad remota debe ser mapeada por el nombre de este campo. 
    private Set<Category> categories = new HashSet<>();

    public void setNotes(Notes notes) {
        this.notes = notes;
        notes.setRecipe(this);
    }

    public Recipe addIngredient(Ingredient ingredient){
        ingredient.setRecipe(this);
        this.ingredients.add(ingredient);
        return this;
    }
}
