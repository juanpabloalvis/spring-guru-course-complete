package com.spring.guru.course.recipe.recipe.webapp.repository;

import com.spring.guru.course.recipe.recipe.webapp.domain.Recipe;
import org.springframework.data.repository.CrudRepository;

public interface RecipeRepository extends CrudRepository<Recipe, Long> {
}
