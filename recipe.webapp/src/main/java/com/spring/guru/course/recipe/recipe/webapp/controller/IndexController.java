package com.spring.guru.course.recipe.recipe.webapp.controller;

import com.spring.guru.course.recipe.recipe.webapp.domain.Category;
import com.spring.guru.course.recipe.recipe.webapp.domain.UnitOfMeasure;
import com.spring.guru.course.recipe.recipe.webapp.repository.CategoryRepository;
import com.spring.guru.course.recipe.recipe.webapp.repository.UnitOfMeasureRepository;
import com.spring.guru.course.recipe.recipe.webapp.service.RecipeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Optional;

@Slf4j
@Controller
public class IndexController {

    private RecipeService recipeService;
    private CategoryRepository categoryRepository;
    private UnitOfMeasureRepository unitOfMeasureRepository;

    public IndexController(RecipeService recipeService, CategoryRepository categoryRepository, UnitOfMeasureRepository unitOfMeasureRepository) {
        this.recipeService = recipeService;
        this.categoryRepository = categoryRepository;
        this.unitOfMeasureRepository = unitOfMeasureRepository;
    }

    @RequestMapping({"", "/", "/index"})
    public String getIndexPage(Model model) {
        log.debug("Getting index page ");

        Optional<Category> categoryOptional = categoryRepository.findByDescription("Mexican");
        Optional<UnitOfMeasure> unitOfMeasure = unitOfMeasureRepository.findByDescription("Teaspoon");

        log.debug("Getting index page ");
        log.debug("Cat id: {}", categoryOptional.isPresent() ? categoryOptional.get().getId() : "Not found");
        log.debug("UOM id: {}", unitOfMeasure.isPresent() ? unitOfMeasure.get().getId() : "Not found");

        model.addAttribute("recipes", recipeService.getRecipes());
        return "index";
    }
}
