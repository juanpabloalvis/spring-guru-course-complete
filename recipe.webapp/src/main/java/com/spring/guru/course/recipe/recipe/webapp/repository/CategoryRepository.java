package com.spring.guru.course.recipe.recipe.webapp.repository;

import com.spring.guru.course.recipe.recipe.webapp.domain.Category;
import com.spring.guru.course.recipe.recipe.webapp.domain.Recipe;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CategoryRepository extends CrudRepository<Category, Long> {
    Optional<Category> findByDescription(String description);
}
