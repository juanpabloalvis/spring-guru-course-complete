package com.spring.guru.course.recipe.recipe.webapp.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToOne;

@Data
@EqualsAndHashCode(exclude = {"recipe"})
@Entity
public class Notes {
    
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    // No especificamos tipo de cascada all, porque al 
    // eliminar una nota, no queremos que elimine las recetas, es 
    // decir no queremos una operacion en cascada
    @OneToOne
    private Recipe recipe;
    
    @Lob 
    private String recipeNotes;

}
