package com.spring.guru.course.recipe.recipe.webapp.domain;

public enum Difficulty {
    EASY, MODERATE, HARD
}
