package com.spring.guru.course.recipe.recipe.webapp.service;

import com.spring.guru.course.recipe.recipe.webapp.domain.Recipe;

import java.util.Set;

public interface RecipeService {
    
    Set<Recipe> getRecipes();

    Recipe findById(Long id);
}
